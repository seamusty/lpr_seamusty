#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "CUnit.h"
#include "Basic.h"
#include "book.h"

//// TESTS
void test00(void){
  struct Recipe* expected = malloc(sizeof(expected));
  expected->name = "waffles";
  expected->serv = 24;
  struct Recipe* actual = newRecipe("waffles", 24);
  CU_ASSERT_STRING_EQUAL( actual->name, expected->name );
  CU_ASSERT_EQUAL( actual->serv, expected->serv );  
}

void test01(void){
  struct Recipe* expected = malloc(sizeof(expected));
  expected->name = "pancakes";
  expected->serv = 32;
  struct Recipe* actual = newRecipe("pancakes", 32);
  CU_ASSERT_STRING_EQUAL( actual->name, expected->name );
  CU_ASSERT_EQUAL( actual->serv, expected->serv );
}

void test02(void){
  struct Recipe* expected = malloc(sizeof(expected));
  expected->name = "french toast";
  expected->serv = 42;
  struct Recipe* actual = newRecipe("french toast", 42);
  CU_ASSERT_STRING_EQUAL( actual->name, expected->name );
  CU_ASSERT_EQUAL( actual->serv, expected->serv );
}

void test03(void){
  struct Recipe* expected = malloc(sizeof(expected));
  expected->name = "cheesburger";
  expected->serv = 1000;
  struct Recipe* actual = newRecipe("cheesburger", 1000);
  CU_ASSERT_STRING_EQUAL( actual->name, expected->name );
  CU_ASSERT_EQUAL( actual->serv, expected->serv );
}

void test04(void){
  struct Recipe* expected = malloc(sizeof(expected));
  expected->name = "cheesburger";
  expected->serv = 1000;
  struct Recipe* actual = newRecipe("cheesburger", 1000);
  CU_ASSERT_STRING_EQUAL( actual->name, expected->name );
  CU_ASSERT_EQUAL( actual->serv, expected->serv );
}

void test05(void){
  struct Recipe* expected = malloc(sizeof(expected));
  expected->name = "awesome sauce";
  expected->serv = 9001;
  struct Recipe* actual = newRecipe("awesome sauce", 9001);
  CU_ASSERT_STRING_EQUAL( actual->name, expected->name );
  CU_ASSERT_EQUAL( actual->serv, expected->serv );
}

int main()
{
   CU_pSuite pSuite = NULL;

   /* initialize the CUnit test registry */
   if (CUE_SUCCESS != CU_initialize_registry()) { return CU_get_error(); }

   /* add a suite to the registry */
   pSuite = CU_add_suite("Suite_1", NULL, NULL);
   if (NULL == pSuite) {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* add the tests to the suite */
   if (
          (NULL == CU_add_test(pSuite, "test00", test00))
        ||(NULL == CU_add_test(pSuite, "test01", test01))
        ||(NULL == CU_add_test(pSuite, "test02", test02))
        ||(NULL == CU_add_test(pSuite, "test03", test03))
        ||(NULL == CU_add_test(pSuite, "test04", test04))
        ||(NULL == CU_add_test(pSuite, "test05", test05))

      )
   {
	   CU_cleanup_registry();
      return CU_get_error();
   }

   /* Run all tests using the CUnit Basic interface */
   CU_basic_set_mode(CU_BRM_VERBOSE);
   CU_basic_run_tests();
   //   CU_basic_show_failures(CU_get_failure_list());
         CU_cleanup_registry();
            return CU_get_error();
}
   

