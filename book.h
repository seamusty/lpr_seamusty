#ifndef BOOK_H
#define BOOK_H
#include <stdlib.h>
#include <string.h>

//struct Book{
//  struct Book* recipes[];
//}; 

struct Recipe{
  char* name;
  char** ingr;
  int serv;     
};

//struct Book* newBook();

struct Recipe* newRecipe(char* name, int servings);

void addIngredient(struct Recipe* recipe, char* ingredient, int quantity);

void deleteRecipe(struct Recipe* recipe); 
#endif
           
