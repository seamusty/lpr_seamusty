OS := $(shell uname -s)

ifeq ($(OS), Darwin)
  CUNIT_PATH_PREFIX = /usr/local/Cellar/cunit/2.1-3/
  CUNIT_DIRECTORY = cunit
endif
ifeq ($(OS), Linux)
  CUNIT_PATH_PREFIX = /util/CUnit/
  CUNIT_DIRECTORY = CUnit/
endif

objects = main.o book.o

CC = gcc
FLAGS = -g -c -Wall -fprofile-arcs -ftest-coverage -std=c11

main : $(objects) main.c
	$(CC) -g -Wall -pg -fprofile-arcs -ftest-coverage -o main $(objects) -lgcov

main.o : main.c
	$(CC) $(FLAGS) main.c -lgcov

book.o : book.h book.c
	$(CC) $(FLAGS) book.c -lgcov

tests.o : tests.c
	$(CC) -c $(FLAGS) -I $(CUNIT_PATH_PREFIX)include/$(CUNIT_DIRECTORY) tests.c -lgcov

tests : book.o tests.o
	gcc -g -Wall -L $(CUNIT_PATH_PREFIX)lib -I $(CUNIT_PATH_PREFIX)includes/$(CUNIT_DIRECTORY) -lm -o tests book.o tests.o -lcunit -lgcov

.PHONY: clean
clean :
	rm -fv main tests *.o *.gcno *.gcda *~ *.out

