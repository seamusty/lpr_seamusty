#include <stdlib.h>
#include <string.h>
#include "book.h"

int main(){
  struct Recipe* beanz = newRecipe("cool beans", 12);
  struct Recipe* veryBeanz = newRecipe("very cool beans", 24); 
  deleteRecipe(beanz);
  deleteRecipe(veryBeanz); 
}
